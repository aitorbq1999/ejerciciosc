#include <stdio.h>
 
int main(){
   
    int a;
    float minutos, horas, dias;
    
    printf("Dame una cantidad de segundos\n");
    scanf("%d", &a);
    getchar();
    
    minutos = a / 60;
    horas = a / 3600;
    dias = a / 86400;
    
    printf("Son %0.9f minutos\n",minutos);
    printf("Son %0.9f horas\n",horas);
    printf("Son %0.9f dias\n",dias);
    
    getchar();
    return 0;
}
    