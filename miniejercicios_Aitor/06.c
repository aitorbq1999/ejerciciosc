#include <stdio.h>
 
int main(){
   
    int a;
    float m, km;
    
    printf("Dame un numero\n");
    scanf("%d", &a);
    getchar();
    
    m = a / 100;
    km = a / 100000;
    
    printf("Son %0.2f metros\n",m);
    printf("Son %0.5f kilometros\n",km);
    
    getchar();
    return 0;
}
    
   
