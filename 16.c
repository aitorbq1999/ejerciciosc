#include <stdio.h>
 
int IsPrime(int num);
 
bool IsNegative(int num);
 
int main(){
   
    int a = 100;
    int b = -300;
 
    if(IsNegative(a)){
        printf("Es negativo a\n");
    }
   
    if(IsNegative(b)){
        printf("Es negativo b\n");
    }
   
 
    getchar();
   
    return 0;
}
 
bool IsNegative(int num){
    return num<0;
}