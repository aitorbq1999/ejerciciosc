#include <stdio.h>
 
int main(){
   
    const float PI = 3.1415f;
    float radio, diametro, circ, area;
    
    printf("Dame el radio de la circumferencia\n");
    scanf("%f", &radio);
    getchar();
    
    diametro = 2*radio;
    circ = 2 * radio * PI;
    area = PI * radio * radio;
    
    printf("El diametro es %.1f\n",diametro);
    printf("El circ es %.1f\n",circ);
    printf("El area es %.1f\n",area);
    
    getchar();
    return 0;
}
    
   
