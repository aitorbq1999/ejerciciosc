#include <stdio.h>
 
/* function declaration */
int max(int num1, int num2);
int min(int num1, int num2);
int add(int num1, int num2);
int mul(int num1, int num2);
 
int main () {
 
   /* local variable definition */
   int a = 100;
   int b = 200;
   int ret;
 
   /* calling a function to get max value */
   ret = max(a, b);
   printf( "Max value is : %d\n", ret );
   
   /* calling a function to get min value */
   ret = min(a, b);
   printf( "Min value is : %d\n", ret );
   
   /* calling a function to get sum value */
   ret = add(a, b);
   printf( "Sum value is : %d\n", ret );
   
   /* calling a function to get mul value */
   ret = mul(a, b);
   printf( "Mult value is : %d\n", ret );
 
   getchar();
   return 0;
}
 
/* function returning the max between two numbers */
int max(int num1, int num2) {
 
   /* local variable declaration */
   int result;
 
   if (num1 > num2)
      result = num1;
   else
      result = num2;
 
   return result;
}
 
int min(int num1, int num2) {
   if (num1 > num2){
      return num2;
   }else{
      return num1;
   }
}
 
int add(int num1, int num2) {
    return num1 + num2;
}
 
int mul(int num1, int num2) {
   return num1 * num2;
}