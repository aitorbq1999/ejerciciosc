#include <stdio.h>
 
int main(){
   
    int numerosFibonacci;
    int contador;
    int antepenultimo, penultimo, actual;
 
    printf("Cuantos numeros de fibonacci quieres?\n");
    scanf("%d", &numerosFibonacci);
    getchar();
 
    antepenultimo = 0;
    penultimo = 1;
   
    printf("%d %d ",antepenultimo,penultimo);
   
    contador=0;
    while(contador<numerosFibonacci){
      actual = antepenultimo + penultimo;
      printf("%d ",actual);
      antepenultimo = penultimo;
      penultimo = actual;
      contador++;
    }
   
    getchar();
   
    return 0;
}
        