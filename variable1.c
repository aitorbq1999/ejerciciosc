#include <stdio.h>

int main()
{
    //Definition
    int a, b;
    int c;
    
    //Initialization
    a = 55;
    b = -20;
    
    c = a - b;
    
    printf("El resultado de %i menos %i es %i \n", a, b, c);
    
    getchar();
    
    return 0;
}