/*
    Types
    
        char: character
        int: integer (numero entero)
        float: foating point value (numero decimal)
    
    Definition
        
        format:
        
            type variableName;
        
        example:
        
            int num;
            char c;
            float num2;
            
            int num3, num4;
            
    Initialization
    
        format:
        
            type variableName = value;            
            variableName = value;
            
        example:
        
            int num = 1;
            
            int num2;
            num2 = 2;
            
            int num3 = 3, num4 = 4;        
*/

#include <stdio.h>

int main()
{
    //Definition
    int a, b;
    int c;
    float f;
    
    //Initialization
    a = 10;
    b = 20;
    
    c = a + b;
    
    printf("c result: %i \n", c);
    
    f = 30.0f/20.f;
    printf("f result: %f \n", f);
    
    getchar();
    
    return 0;
}